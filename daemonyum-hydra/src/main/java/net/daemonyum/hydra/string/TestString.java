package net.daemonyum.hydra.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Kiyous
 *
 */
public class TestString {
	
	public static boolean isNull(Object o) {
		return o == null;
	}
	
	public static boolean isNotNull(Object o) {
		return o != null;
	}
	
	public static boolean isNotEmpty(Object o) {
		return isNotNull(o) && isNotEmpty(o.toString());
	}
	
	public static boolean isNotEmpty(String s) {
		return isNotNull(s) && s.trim().length() > 0;
	}
	
	public static boolean isEmpty(Object o) {
		return isNull(o) || isEmpty(o.toString());
	}
	
	public static boolean isEmpty(String s) {
		return isNull(s) || s.trim().length() == 0;
	}
	
	public static boolean areNotEmpty(String...string) {
		for(String s : string) {
			if(isEmpty(s)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean areNotEmpty(Object...o) {
		return isNotNull(o) && areNotEmpty(o.toString());
	}
	
	public static boolean maxLength(int max,String string) {
		if(string != null) {
			return string.length() <= max;
		}
		return false;
	}
	
	public static boolean minLength(int min,String string) {
		if(string != null) {
			return string.length() >= min;
		}
		return false;
	}
	
	public static boolean maxLength(int max, Object o) {
		return isNotNull(o) && maxLength(max, o.toString());
	}
	
	public static boolean minLength(int min, Object o) {
		return isNotNull(o) && minLength(min, o.toString());
	}
	
	public static boolean isNumeric(String string) {
		if(string != null) {
			Pattern p = Pattern.compile("[0-9].*");
		 	Matcher m = p.matcher(string);
		 	if(m.matches()) {
		 		return true;
		 	}
		}
	 	return false;
	}
	
	public static boolean isNumeric(Object o) {
		return isNotNull(o) && isNumeric(o.toString());
	}
	
	public static boolean isAlphabetic(String string) {
		if(string != null) {
			Pattern p = Pattern.compile("[a-zA-Z].*");
		 	Matcher m = p.matcher(string);
		 	if(m.matches()) {
		 		return true;
		 	}
		}
	 	return false;
	}
	
	public static boolean isAlphabetic(Object o) {
		return isNotNull(o) && isAlphabetic(o.toString());
	}
	
	public static boolean isAlphanumeric(String string) {
		if(string != null) {
			Pattern p = Pattern.compile("[a-zA-Z0-9].*");
		 	Matcher m = p.matcher(string);
		 	if(m.matches()) {
		 		return true;
		 	}
		}
	 	return false;
	}
	
	public static boolean isAlphanumeric(Object o) {
		return isNotNull(o) && isAlphanumeric(o.toString());
	}
	
	public static boolean isValid(String regexp, String string) {
		
		if(string != null) {
			Pattern p = Pattern.compile(regexp);
		 	Matcher m = p.matcher(string);
		 	if(m.matches()) {
		 		return true;
		 	}
		}
	 	return false;
	}
	
	public static boolean isValid(String regexp, Object o) {
		return isNotNull(o) && isValid(regexp, o.toString());
	}
	
}
