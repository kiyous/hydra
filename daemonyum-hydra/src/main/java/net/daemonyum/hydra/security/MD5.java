package net.daemonyum.hydra.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Chiffre une chaine de caractère en MD5
 * @author Kiyous
 *
 */
public class MD5 {
	
	public static String encode(String toEncode) {
		if(toEncode != null) {
			byte[] uniqueKey = toEncode.getBytes();
			byte[] hash = null;
	
			try {
				hash = MessageDigest.getInstance("MD5").digest(uniqueKey);
			}catch (NoSuchAlgorithmException e) {
				throw new Error("No MD5 support in this VM.");
			}
	
			StringBuilder hashString = new StringBuilder();
			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(hash[i]);
				if (hex.length() == 1) {
					hashString.append('0');
					hashString.append(hex.charAt(hex.length() - 1));
				}else {
					hashString.append(hex.substring(hex.length() - 2));
				}
			}
			return hashString.toString();
		}
		return null;
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Usage: java MD5 <string_to_encode>");
			return;
		}
		String toEncode = args[0];
		System.out.println("Original string ... " + toEncode);
		System.out.println("String MD5 ........ " + encode(toEncode));
	}
}

