package net.daemonyum.hydra.logger;

/**
 * 
 * @author Kiyous
 *
 */
public class Logger extends org.apache.log4j.Logger{

	protected Logger(String name) {
		super(name);		
	}
	
	
	@Override
	public void debug(Object message) {
		if(isDebugEnabled()) {
			super.debug(message);
		}
	}

}
