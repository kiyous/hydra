package net.daemonyum.hydra.thread;

public interface BackgroundAction {

	public void runInBackground();

}
