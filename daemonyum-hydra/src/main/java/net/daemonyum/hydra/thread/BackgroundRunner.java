package net.daemonyum.hydra.thread;

import org.apache.log4j.Logger;

public class BackgroundRunner {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(BackgroundRunner.class);

	private Thread thread;

	public BackgroundRunner(final BackgroundAction action) {

		Runnable runner = new Runnable() {
			@Override
			public void run() {
				action.runInBackground();
			}
		};
		thread = new Thread(runner);
		thread.start();
	}

}
